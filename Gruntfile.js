module.exports = function (grunt) {

    var COPY_RIGHT_BANNER =
        '/***********************************************\n' +
        ' *            Copy Right Goes Here             *\n' +
        ' ***********************************************/\n';

    require('load-grunt-tasks')(grunt);

    grunt.initConfig({

        /**
         * Compiles our TS into JS
         */
        ts: {
            dist: {
                src: ['app/src/**/*.ts', '!node_modules/**/*.ts', '!bower_components/**/*.ts'],
                options: {
                    module: "commonjs"
                }
            },
            dev: {
                src: ['app/src/**/*.ts', '!node_modules/**/*.ts', '!bower_components/**/*.ts'],
                options: {
                    module: "commonjs"
                }
            }
        },

        /**
         *  Turns our commonjs modules into one usable by the client. Also compiles all the JS into one file.
         *  In dev mode it will keep maps back to the src TS so it can be debugged on the server.
         */
        browserify: {
            dev: {
                // A single entry point for our app
                src: 'app/src/js/main.js',
                // Compile to a single file to add a script tag for in your HTML
                dest: 'app/dev/js/main.js',

                options: {
                    browserifyOptions: {
                        debug: true
                    },
                    preBundleCB: function (bundle) {
                        bundle.plugin('tsify');
                    }
                }
            },
            dist: {
                // A single entry point for our app
                src: 'app/src/js/main.js',
                // Compile to a single file to add a script tag for in your HTML
                dest: 'app/dist/js/main.js',
                options: {
                    preBundleCB: function (bundle) {
                        bundle.plugin('tsify');
                    }
                }
            }
        },

        /**
         *  Minifies the JS for distribution.
         */
        uglify: {
            options: {
                banner: COPY_RIGHT_BANNER + '\n' //this one needs an extra new line.
            },
            dist: {
                files: {
                    'app/dist/js/main.js': ['app/dist/js/main.js']
                }
            }
        },

        /**
         *  Turns the less into css
         */
        less: {
            dev: {
                files: [{
                    expand: true,
                    cwd: 'app/src/less',
                    src: ['main.less'],
                    dest: 'app/dev/css/',
                    ext: '.css'
                }]
            },
            dist: {
                files: [{
                    expand: true,
                    cwd: 'app/src/less',
                    src: ['main.less'],
                    dest: 'app/dist/css/',
                    ext: '.css'
                }]
            }
        },

        /**
         * Minifies the css for distribution
         */
        cssmin: {
            dist: {
                options: {
                    banner: COPY_RIGHT_BANNER
                },
                files: {
                    'app/dist/css/main.css': ['app/dist/css/main.css']
                }
            }
        },

        /**
         * Watches the files for changes and than triggers tasks if the changes happened.
         */
        watch: { //todo expand this out so when you only edit less it only runs less req things
            dev: {
                options: {
                    livereload: true
                },
                files: [
                    'app/src/**/*.less',
                    'app/src/**/*.html',
                    'app/assets/stylesheets/*.less',
                    'app/src/**/*.ts'
                ],
                tasks: ['less:dev', 'ts:dev', 'browserify:dev', 'copy:dev']
            },
            prodTest:{
                options: {
                    livereload: true
                },
                files: [
                    'app/src/**/*.less',
                    'app/src/**/*.html',
                    'app/assets/stylesheets/*.less',
                    'app/src/**/*.ts'
                ],
                tasks: ['less:dist', 'cssmin:dist', 'ts:dist', 'browserify:dist', 'uglify:dist', 'copy:dist']
            }
        },

        /**
         * Copies the html file into the right location based on if it is dev or production
         */
        copy: {
            dist: {
                expand: true,
                cwd: 'app/src/',
                src: ['**/*.html'],
                dest: 'app/dist/'
            },
            dev: {
                expand: true,
                cwd: 'app/src/',
                src: [
                    '**/*.html'
                ],
                dest: 'app/dev/'
            }
        },

        /**
         * Front End server for development and testing production without a real server
         */
        connect: {
            dev: {
                options: {
                    base: 'app/dev/',
                    port: 8000,
                    hostname: 'localhost',
                    directory: 'app/dev/'
                }
            },
            prodTest:{
                options: {
                    base: 'app/dist/',
                    port: 8000,
                    hostname: 'localhost',
                    directory: 'app/dist/'
                }
            }
        }


    });

    /**
     * Used for developing our app, it includes maps back to the source ts for easier debugging
     */
    grunt.registerTask('default', ['less:dev', 'ts:dev', 'browserify:dev', 'copy:dev', 'connect:dev', 'watch:dev']);
    /**
     * Used to build the front end for production
     */
    grunt.registerTask('build', ['less:dist', 'cssmin:dist', 'ts:dist', 'browserify:dist', 'uglify:dist', 'copy:dist']);
    /**
     * Used to test our Production code structure and app
     */
    grunt.registerTask('prodTest', ['less:dist', 'cssmin:dist', 'ts:dist', 'browserify:dist', 'uglify:dist', 'copy:dist', 'connect:prodTest', 'watch:prodTest'])



};

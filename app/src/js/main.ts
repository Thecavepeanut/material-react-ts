/// <reference path="../ws-local-typings/local.d.ts"/>
/// <reference path="../components/mui-button-group/mui-button-group.ts"/>
/// <reference path="../components/ws-button-group/ws-button-group.ts"/>
/// <reference path="../components/ws-mui-toolbar/ws-mui-toolbar.ts"/>

import React = require("react");
import mui = require("material-ui");
import TR = require("typed-react");
import injectTapEventPlugin = require("react-tap-event-plugin");
import wsToolBar = require("../components/ws-mui-toolbar/ws-mui-toolbar");

//init injectTapEvent for iOS.
injectTapEventPlugin();

export module ReactBoiler {
    var menu_test_items:wsToolBar.MenuItem[] = [
            {route: 'home', text: 'Home'},
            {route: 'sort', text: 'Sort'},
            {route: 'filter', text: 'Filter'}],
        right_test_icons:wsToolBar.Icon[] = [
            {src: 'action-search'},
            {src: 'action-print'},
            {src: 'action-info'}
        ];

    export var start = () => {
        wsToolBar.render({
                menuItems: menu_test_items,
                key:'ws-tool-bar',
                title: 'React w/o JSX, TypeScript, and Material UI Boiler',
                rightIcons: right_test_icons
            }
            , document.getElementById('header'));
    }
}

ReactBoiler.start();

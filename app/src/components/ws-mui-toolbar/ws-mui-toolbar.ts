/// <reference path="../../js/main.ts"/>

import React = require("react");
import mui = require("material-ui");
import TR = require("typed-react");

var Icon = mui.Icon;
var Toolbar = mui.Toolbar;
var ToolbarGroup = mui.ToolbarGroup;
var LeftNav = mui.LeftNav;

export interface MenuItem {
    text:string;
    route?:string;
    onTouchTap?:()=>any;
}

export interface ToolBarProps {
    menuItems:MenuItem[];
    title:string;
    rightIcons:Icon[];
}

export interface Icon {
    src:string;
    onTouchTap?:()=>any;
}

export interface ToolBarState {
    leftNavOpen:boolean;
}

class WsToolbar extends TR.Component <ToolBarProps,ToolBarState>{
    render() {

        var rIcons = this.props.rightIcons.map((icon) => {
            return React.createElement(Icon, {
                icon: icon.src,
                className:'ws-icon-right',
                onTouchTap:typeof icon.onTouchTap === "function" ? icon.onTouchTap : () => {}});
        });

        return (
            React.createElement("div", {className: "ws-toolbar"},
                React.createElement(Toolbar, null,
                    React.createElement(ToolbarGroup, {key: 0, float: "left"},
                        React.createElement(Icon, {icon: "navigation-menu", onTouchTap: this._toggleLeftNav, className:"ws-left-nav-btn"},
                            React.createElement(LeftNav, {
                                ref: "leftNav",
                                className:"ws-left-nav",
                                docked: false,
                                menuItems: this.props.menuItems
                            }))
                    ),
                    React.createElement(ToolbarGroup, {key: 1, float: "right"}, rIcons),
                    React.createElement('div', {className: 'ws-toolbar-title', float:"left"},this.props.title)
                )
            )
        )
    }

    private _toggleLeftNav = () => {
        this.refs['leftNav']['toggle']();
    }
}

var toolBar = TR.createClass(WsToolbar);

export var render = (props:ToolBarProps, el:Element, cb = () => null) => {
    React.render(React.createElement(toolBar, props), el, cb);
};
/// <reference path="../../js/main.ts"/>
import React = require("react");
import TR = require("typed-react");

export interface ButtonProps {
    text:string;
    key:string;
    className?:string;
    onClick?:() => any;
}

export interface ButtonState {
    focused: boolean;
    active: boolean;
}

export interface BtnGroupProps {
    btns: ButtonProps[];
}

export interface BtnGroupState {
    btns: ButtonState[];
}

class WsBtn extends TR.Component<ButtonProps, ButtonState> {
    render() {
        return React.createElement("button", {key: this.props.key}, this.props.text);
    }
}

class WsBtnGroup extends TR.Component<BtnGroupProps, BtnGroupState> {
    render() {

        var btns = this.props.btns.map((b)=> {
            return React.createElement(wsButton, {key: b.key, text: b.text});
        });

        return React.createElement('div', {className: 'ws-btn-group'}, btns);
    }
}
var wsButton = TR.createClass(WsBtn);
var wsBtnGroup = TR.createClass(WsBtnGroup);

export var render = (btns:ButtonProps[], el:Element, cb = Function) => {
    if (btns.length !== 0) {
        React.render(React.createElement(wsBtnGroup, {btns: btns}), el, cb);
    }
    else {
        console.log('can not give muiBtnGroup an empty button array')
    }
};

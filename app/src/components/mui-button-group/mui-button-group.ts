/// <reference path="../../js/main.ts"/>
import React = require("react");
import mui = require("material-ui");
import TR = require("typed-react");

export interface ButtonProps {
    text:string;
    key:string;
    className?:string;
    onClick?:() => any;
}

export interface ButtonState {
    focused: boolean;
    active: boolean;
}

export interface BtnGroupProps {
    btns: ButtonProps[];
}

export interface BtnGroupState {
    btns: ButtonState[];
}

class MuiBtnGroup extends TR.Component<BtnGroupProps, BtnGroupState> {
    render() {
        var btns = this.props.btns.map((b)=> {
            return React.createElement(mui.RaisedButton, {label: b.text, key: b.key})
        });
        return React.createElement('div', {className: 'mui-btn-group'}, btns);
    }

}

var muiBtnGroup = TR.createClass(MuiBtnGroup);

export var render = (btns:ButtonProps[], el:Element, cb = Function) => {
    if (btns.length !== 0) {
        React.render(React.createElement(muiBtnGroup, {btns: btns}), el, cb);
    }
    else {
        console.log('can not give muiBtnGroup an empty button array')
    }
};


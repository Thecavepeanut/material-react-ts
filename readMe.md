React Material Typescript Boiler Plate
===

Required Global Tools
----
- GIT: if you don't know what git is well ... [Git Install](http://lmgtfy.com/?q=git)

- NPM: Node package manager used to manager our packages used by the JS. [NPM Install](https://docs.npmjs.com/getting-started/installing-node). For linux do sudo apt-get install node.

- Grunt: Task runner for JS projects: <br> `sudo npm i -g grunt-cli`

- TSD: Package manager for .d.ts files for the TypeScript <br>`sudo npm i -g tsd@next` <br><small><strong> Note:</strong> Must have at least version 0.6 to add the node_module crawling to tsd so it will reference all the .d.ts files in the node_modules dir. Right now (12/22/14) you must use the tsd@next to get the right version.</small>
 
Libraries Used
---
### Development ####

- <strong>Browserify</strong>: Used to compile commonjs (npm) modules and our source code into a single js file and add a Node style require.  This will also set up a map file, if in dev mode, so we can debug in the browser with ease.
 
- <strong>grunt</strong>: Your typical JS task runner.  We chose this one because it is easy to use and Tim was already familiar with it.  At some point we may want to swap to gulp but I see no point too atm.
 
- <strong>grunt-browserify</strong>: so we can use browserify with grunt.
 
- <strong>grunt-contrib-connect</strong>: so we can run a simple python server for quick development w/o the play2 framework.
 
- <strong>grunt-contrib-cssmin</strong>: used to minify our css in our grunt task for a production build.
 
- <strong>grunt-contrib-less</strong>: Used to compile less in the grunt task runner.
 
- <strong>grunt-contrib-uglify</strong>: Used to minify our JS code in our grunt task runner for production builds.
 
- <strong>grunt-contrib-watch</strong>: Used to watch our files and if any of them change to re run the appropriate tasks. Aka reload as you go.
 
- <strong>grunt-contrib-cop</strong>: Used to copy any files we need to a new path.
 
- <strong>grunt-ts</strong> : Grunt task runner for compiling typescript.
 
- <strong> load-grunt-tasks </strong>: Used to load all of our grunt tasks, rather than us write them all out.
 
 
### Dependencies
- <strong> lodash </strong>: A great library for modifying, finding, etc collection in JS.  This is like the hammer of my JS toolbelt.
- <strong> Material UI </strong>: An implementation of google material design concept done in react.
- <strong> react </strong>: What seems to be the best of the "web components" type framework, that works well with iOS.  Well at least how we are using it.
- <strong> react-tap-event-plugin </strong>: A plugin for react to help with how stupid iOS is.
- <strong> typed-react </strong>: A wrapping library and definition file so we can use react and typescript together.
 
 
 ### Copy Right
 This code is hear by under the you owe me a beer and if you have cookies you have to give me one too copyright.
  There are no warranties nor assurances about this code use at your own risk.  If you do
  choose to use the code and we meet in a setting that sells beer you must buy the author of
  this code at least one. Furthermore if you and the author of this code ever met in a situation
  where you have cookies in your possession you must give the author of this code at least one,
  if the author quite enjoys the first you must give the author one more if you are able.